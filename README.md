# portfolio

Faire votre portfolio pour le site simplonlyon.fr dans lequel devront figurés :
* Une petite présentation
* La liste de vos projets
* Un contact
* Le lien vers votre gitlab
* Eventuellement vos préférences en terme de langages/techno

Le site devra être responsive et valide W3C. Il est fortement conseillé d'utiliser la grid et les components bootstraps.

## Conception
Le projet devra commencer par avoir : 
* 3-4 user stories ou plus de votre site portfolio (histoire de définir qui utilisera le site et pourquoi)
* La ou les maquettes fonctionnelles du site (qui doivent en gros indiquée les éléments d'interface et leur positionnements)
* Un projet gitlab avec un README qui décrit le projet puis qui contiendra à terme vos user stories et vos maquettes
* Faire la structure HTML complète de l'application avant de commencer le style

## Mise en ligne

Pour mettre le site en ligne, il faudra vous connecter en ssh au serveur simplonlyon.fr
puis aller dans votre dossier www et y faire un git clone de votre projet.

1. Se connecter en ssh au serveur (`ssh promo12@simplonlyon.fr`)
2. Aller dans le dossier www et dans votre dossier personnel
3. Récupérer le contenu de votre conf.json et créer un conf.json dans votre projet à la racine (au même niveau que le index.html) et mettre le contenu copier dedans, mettre également votre avatar dans le projet (puis commit/push le tout)
4. Vider votre dossier personnel sur le serveur (avec des rm)
5. Cloner votre projet en https dans le dossier courant avec `git clone https://gitlab.com/username/portfolio .`